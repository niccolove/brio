import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PC3
import org.kde.kirigami 2.13 as Kirigami

Item {
    property bool first: false
    property bool second: false
    property bool last: false
    property bool collapsed: false
    Layout.preferredWidth: PlasmaCore.Units.gridUnit * 2
    Layout.fillHeight: true

    Rectangle {
        visible: first
        anchors {
            verticalCenter: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter
        }
        width: PlasmaCore.Units.mediumSpacing * 2 - 1
        height: PlasmaCore.Units.mediumSpacing * 2 - 1
        color: Kirigami.Theme.textColor
    }

    Rectangle {
        anchors {
            top: first ? parent.verticalCenter : parent.top
            bottom: last ? parent.verticalCenter : parent.bottom
            topMargin: first ? 0 : -mainGrid.rowSpacing - (second ? parent.height / 2 : 0)
        }
        x: Math.round(parent.width / 2) - 1
        width: 1
        color: Kirigami.Theme.textColor
        visible: !collapsed
    }

    Rectangle {
        visible: !first
        anchors {
            left: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }
        height: 1
        width: PlasmaCore.Units.mediumSpacing * 2
        color: Kirigami.Theme.textColor
    }

    PC3.Button {
        anchors.fill: parent
        visible: first
        onPressed: collapsed = !collapsed
        flat: true
    }
}
