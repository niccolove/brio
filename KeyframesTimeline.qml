import QtQuick 2.15
import QtQuick.Controls 2.15
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.kirigami 2.13 as Kirigami

Item {
    id: root
    property var keyframes: []
    property bool locked: false
    property string name
    property string prop
    height: PlasmaCore.Units.gridUnit
    width: timelineStuff.totalWidth

    Rectangle {
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            right: parent.right
        }
        height: 1
        color: Kirigami.Theme.disabledTextColor
    }

    Repeater {
        model: keyframes

        Rectangle {
            id: main
            anchors.verticalCenter: parent.verticalCenter
            x: modelData * timelineStuff.frameWidth - width / 2
            color: Kirigami.Theme.backgroundColor
            border.color: Kirigami.Theme.textColor
            border.width: 1
            height: root.locked ? 6 : 10
            width: height
            transform: Rotation {
                angle: 45
                origin.x: width / 2
                origin.y: height / 2
            }
            property int thisFrame: modelData

            MouseArea {
                visible: !root.locked
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                drag.target: parent
                drag.minimumX: 0
                drag.maximumX: timelineStuff.totalWidth

                onClicked: {
                    if (mouse.button & Qt.RightButton)
                        menuLoader.sourceComponent = menuTemplate;
                    else
                        timelineStuff.time = modelData;
                }
                onReleased: {
                    let newFrame = Math.round(((parent.x + parent.width / 2) / timelineStuff.frameWidth))
                    backend.moveKeyframe(name, prop, parent.thisFrame, newFrame)
                    parent.thisFrame = newFrame
                    parent.x = Qt.binding(function() {return newFrame * timelineStuff.frameWidth - width / 2})
                }

                Loader {
                    id: menuLoader
                    property bool visibleContent: item.visible
                    onVisibleContentChanged: {
                        if (!visibleContent) sourceComponent = undefined;
                    }
                    onLoaded: item.popup()
                }

                Component {
                    id: menuTemplate
                    Menu {
                        Menu {
                            title: "Easing Curve..."

                            MenuItem {
                                text: "Linear"
                                onTriggered: backend.setEasingCurve(name, prop, modelData, 'LinearInOut')
                            }

                            Menu {
                                title: "Quad"
                                MenuItem {
                                    text: "In"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'QuadEaseIn')
                                }
                                MenuItem {
                                    text: "Out"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'QuadEaseOut')
                                }
                                MenuItem {
                                    text: "InOut"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'QuadEaseInOut')
                                }
                            }
                            Menu {
                                title: "Cubic"
                                MenuItem {
                                    text: "In"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'CubicEaseIn')
                                }
                                MenuItem {
                                    text: "Out"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'CubicEaseOut')
                                }
                                MenuItem {
                                    text: "InOut"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'CubicEaseInOut')
                                }
                            }
                            Menu {
                                title: "Quartic"
                                MenuItem {
                                    text: "In"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'QuarticEaseIn')
                                }
                                MenuItem {
                                    text: "Out"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'QuarticEaseOut')
                                }
                                MenuItem {
                                    text: "InOut"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'QuarticEaseInOut')
                                }
                            }
                            Menu {
                                title: "Elastic"
                                MenuItem {
                                    text: "In"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'ElasticEaseIn')
                                }
                                MenuItem {
                                    text: "Out"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'ElasticEaseOut')
                                }
                                MenuItem {
                                    text: "InOut"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'ElasticEaseInOut')
                                }
                            }
                            Menu {
                                title: "Bounce"
                                MenuItem {
                                    text: "In"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'BounceEaseIn')
                                }
                                MenuItem {
                                    text: "Out"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'BounceEaseOut')
                                }
                                MenuItem {
                                    text: "InOut"
                                    onTriggered: backend.setEasingCurve(name, prop, modelData, 'BounceEaseInOut')
                                }
                            }
                        }
                        MenuItem {
                            text: "Remove"
                            icon.name: 'edit-delete-remove'
                            onTriggered: backend.removeKeyframe(name, prop, modelData)
                        }
                    }
                }

            }
        }
    }
}
