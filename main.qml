import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQml.Models 2.15
import QtQuick.Dialogs 1.0
import org.kde.kirigami 2.13 as Kirigami
import org.kde.plasma.components 3.0 as PC3
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras

Kirigami.ApplicationWindow {
    id: root

    title: "Brio"
    width: Kirigami.Units.gridUnit * 70
    height: Kirigami.Units.gridUnit * 19
    minimumWidth: Kirigami.Units.gridUnit * 10
    minimumHeight: Kirigami.Units.gridUnit * 5

    Item {
        id: timelineStuff
        property int time: 0
        onTimeChanged: backend.setTime(time)
        property int to: 120
        property int fps: 60
        property real speed: 1
        property real frameWidth: Math.pow(zoomPercentage.value, 2) / 100 * 2
        property int totalWidth: Math.round(frameWidth * to)
        property int tickDistance: Math.round((100 / Math.pow(zoomPercentage.value, 2)) * 20)
    }

    Timer {
        id: playbackTimer
        interval: Math.round(1000 / timelineStuff.fps / timelineStuff.speed)
        repeat: true
        onTriggered: timelineStuff.time = (timelineStuff.time + 1) % timelineStuff.to
    }

    property var importantStuff: [backend.names, backend.properties, backend.keynodes]
    onImportantStuffChanged: {
        backend.setTime(timelineStuff.time)
        backend.save()
    }

    pageStack.initialPage: Kirigami.Page {
        title: "Timeline"

        actions {
            right: Kirigami.Action {
                text: "Render"
                icon.name: "preview-render-on"

                Kirigami.Action {
                    text: "To MP4 without transparency"
                    onTriggered: {
                        backend.render(timelineStuff.to, timelineStuff.fps, timelineStuff.speed, false)
                    }
                }
                Kirigami.Action {
                    text: "To MOV with transparency"
                    onTriggered: backend.render(timelineStuff.to, timelineStuff.fps, timelineStuff.speed, true)
                }
            }
            main: Kirigami.Action {
                text: playbackTimer.running ? "Stop" : "Play"
                icon.name: playbackTimer.running ? "media-playback-stop" : "media-playback-start"
                onTriggered: playbackTimer.running = !playbackTimer.running
            }
            left: Kirigami.Action {
                text: "Add Keyframes"
                icon.name: "keyframe-add"
                onTriggered: backend.addKeyframes(timelineStuff.time)
            }
        }

        titleDelegate: RowLayout {
            PC3.Label {
                text: "Speed:"
            }
            PC3.SpinBox {
                id: speedSpinbox
                from: 10
                value: timelineStuff.speed * 100
                onValueChanged: timelineStuff.speed = value / 100
                to: 500
                stepSize: 5
            }
            Item {
                Layout.preferredWidth: PlasmaCore.Units.largeSpacing
            }
            PC3.Label {
                text: "FPS:"
            }
            PC3.SpinBox {
                from: 30
                value: timelineStuff.fps
                onValueChanged: timelineStuff.fps = value
                to: 120
                stepSize: 30
            }
            Item {
                Layout.preferredWidth: PlasmaCore.Units.largeSpacing
            }
            PC3.Label {
                text: "Total frames:"
            }
            PC3.SpinBox {
                from: 30
                value: timelineStuff.to
                onValueChanged: timelineStuff.to = value
                to: 1200
                stepSize: 10
            }
        }

        ScrollView {
            anchors.fill: parent
            clip: false


            MouseArea {

                function mouseSetValue(x) {
                    let pX = Math.round((x - mainTimeline.x) / timelineStuff.frameWidth)
                    if (pX < 0) return;
                    pX = Math.min(Math.max(pX, 0), timelineStuff.to)
                    timelineStuff.time = pX
                }
                anchors.fill: parent

                onPressed: mouseSetValue(mouseX)
                onPositionChanged: mouseSetValue(mouseX)
            }

            Rectangle {
                x: mainTimeline.x + timelineStuff.frameWidth * timelineStuff.time
                y: 0
                width: 1
                height: mainGrid.height
                color: Kirigami.Theme.textColor
            }

            GridLayout {
                id: mainGrid
                columnSpacing: PlasmaCore.Units.smallSpacing
                rowSpacing: PlasmaCore.Units.mediumSpacing
                columns: 3

                PlasmaExtras.Heading {
                    Layout.columnSpan: 2
                    Layout.rightMargin: PlasmaCore.Units.largeSpacing * 2
                    text: "Timeline"
                }

                MainTimeline {id: mainTimeline}

                TreeItem {
                    first: true
                    visible: backend.selected_el.length
                }
                PlasmaExtras.Heading {
                    level: 3
                    text: backend.selected_el.length == 1 ? backend.selected_el[0] : backend.selected_el.length + " elements"
                    visible: backend.selected_el.length
                    Layout.rightMargin: PlasmaCore.Units.largeSpacing * 2
                }
                KeyframesTimeline {
                    opacity: 0
                    visible: backend.selected_el.length
                }

                TreeItem {
                    last: true
                    visible: backend.selected_el.length
                }
                PC3.Button {
                    text: "Add prop..."
                    onPressed: {
                        addSheet.editingObject = backend.selected_el
                        addSheet.open()
                    }

                    visible: backend.selected_el.length
                }
                KeyframesTimeline {
                    opacity: 0
                    visible: backend.selected_el.length
                }

                Timer { // TODO HORRIBLE HACK: ordering is wrong otherwise. Qt bug?
                    id: hackyTimer
                    running: true
                    interval: 10
                    onTriggered: backend.quickPropRefresh()
                }
                property var hackyTrigger: backend.names
                onHackyTriggerChanged: hackyTimer.running = true

                Repeater {
                    model: backend.names

                    Repeater {
                    id: wrapper
                    property string objectName: modelData

                    model: ObjectModel {

                        TreeItem {
                            first: true;
                            id: mainTree
                            collapsed: !backend.selected_els.includes(wrapper.objectName)
                        }
                        PlasmaExtras.Heading {
                            level: 3
                            text: wrapper.objectName
                            Layout.rightMargin: PlasmaCore.Units.largeSpacing * 2

                            MouseArea {
                                anchors.fill: parent
                                acceptedButtons: Qt.RightButton
                                onClicked: headingContextMenu.popup()
                            }

                            Menu {
                                id: headingContextMenu
                                MenuItem {
                                    text: "Stop animathing object"
                                    icon.name: 'edit-delete-remove'
                                    onTriggered: backend.removeObject(wrapper.objectName)
                                }
                                MenuItem {
                                    text: "Change animated properties..."
                                    onTriggered: {
                                        addSheet.editingObject = wrapper.objectName
                                        addSheet.props = backend.properties[wrapper.objectName]
                                        addSheet.open()
                                    }
                                }
                            }
                        }
                        KeyframesTimeline {
                            opacity: mainTree.collapsed
                            keyframes: Object.values(backend.keynodes[wrapper.objectName]).reduce((acc, list) => acc.concat(list), []);
                            locked: true
                        }

                        Repeater {

                            model: backend.properties[wrapper.objectName]

                            Repeater {
                            id: subwrapper
                            property string objectProp: modelData

                            model: ObjectModel {

                                TreeItem {
                                    second: index == 0
                                    last: index == backend.properties[wrapper.objectName].length - 1
                                    visible: !mainTree.collapsed
                                }
                                PC3.Label {
                                    visible: !mainTree.collapsed
                                    text: subwrapper.objectProp

                                    MouseArea {
                                        anchors.fill: parent
                                        acceptedButtons: Qt.RightButton
                                        onClicked: subContextMenu.popup()
                                    }

                                    Menu {
                                        id: subContextMenu
                                        MenuItem {
                                            text: "Stop animating property"
                                            icon.name: 'edit-delete-remove'
                                            onTriggered: backend.removeProperty(wrapper.objectName, subwrapper.objectProp)
                                        }
                                    }
                                }
                                KeyframesTimeline {
                                    visible: !mainTree.collapsed
                                    keyframes: backend.keynodes[wrapper.objectName][subwrapper.objectProp]
                                    name: wrapper.objectName
                                    prop: subwrapper.objectProp
                                }
                            }

                        } }

                    } }
                }

            }
        }

    }

    Kirigami.OverlaySheet {
        id: addSheet

        property var props: []
        property var editingObject

        component PropCheckBox : PC3.CheckBox {
            checked: addSheet.props.includes(text)
            onCheckedChanged: {
                if (checked && addSheet.props.indexOf(text) < 0) {
                    addSheet.props.push(text)
                } else if (!checked) {
                    addSheet.props.splice(addSheet.props.indexOf(text), 1)
                }
            }
        }

        header: Kirigami.Heading {
            text: i18nc("@title:window", "Select properties to animate:")
        }
        ColumnLayout {
            spacing: PlasmaCore.Units.largeSpacing
            RowLayout {
                spacing: PlasmaCore.Units.largeSpacing
                PlasmaExtras.Heading {
                    level: 3
                    text: "Basics:"
                }
                PropCheckBox {
                    text: "x"
                }
                PropCheckBox {
                    text: "y"
                }
                PropCheckBox {
                    text: "Width"
                }
                PropCheckBox {
                    text: "Height"
                }
            }
            RowLayout {
                spacing: PlasmaCore.Units.largeSpacing
                PlasmaExtras.Heading {
                    level: 3
                    text: "Style:"
                }
                PropCheckBox {
                    text: "Stroke"
                }
                PropCheckBox {
                    text: "Stroke width"
                }
                PropCheckBox {
                    text: "Stroke opacity"
                }
            }
            RowLayout {
                spacing: PlasmaCore.Units.largeSpacing
                PropCheckBox {
                    text: "Fill"
                }
                PropCheckBox {
                    text: "Fill opacity"
                }
                PropCheckBox {
                    text: "Opacity"
                }
                PropCheckBox {
                    text: "Blur"
                }
            }
            RowLayout {
                spacing: PlasmaCore.Units.largeSpacing
                PlasmaExtras.Heading {
                    level: 3
                    text: "Transforms:"
                }
                PropCheckBox {
                    text: "Transform"
                }
            }
            RowLayout {
                spacing: PlasmaCore.Units.largeSpacing
                PlasmaExtras.Heading {
                    level: 3
                    text: "Other:"
                }
                PropCheckBox {
                    text: "Path"
                }
            }
            RowLayout {
                Item {width: PlasmaCore.Units.gridUnit * 20}
                PC3.Button {
                    text: "Apply"
                    onClicked: {
                        if (backend.names.includes(addSheet.editingObject)) {
                            backend.setObjectProperties(addSheet.editingObject, addSheet.props, timelineStuff.time)
                        } else {
                            backend.addObject(addSheet.editingObject, addSheet.props, timelineStuff.time)
                        }
                        addSheet.props = []
                        addSheet.editingObject = ""
                        addSheet.close()
                    }
                }
            }
        }
    }


    footer: RowLayout {

        PC3.Button {
            text: "Load"
            flat: true
            onClicked: backend.load()
        }
        Item {
            Layout.fillWidth: true
        }
        PC3.Label {
            text: "Zoom:"
        }

        PC3.Slider {
            id: zoomPercentage
            from: 4
            to: 44
            value: 18
        }
    }
}
