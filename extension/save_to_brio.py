#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [YEAR] [YOUR NAME], [YOUR EMAIL]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Description of this extension goes here (if nicco will ever bother writing it)
"""

import inkex
import random
from time import sleep
from collections import defaultdict
from os.path import expanduser
from dataclasses import dataclass
from typing import Optional
import base64
from multiprocessing.connection import Client

class NewBrioExtension(inkex.EffectExtension):

    def effect(self):

        conn = Client(('localhost', 6123), authkey=b'brio')
        obj = conn.recv()

        if obj:
            self.svg.set('brio', obj)

        data = self.svg.get('brio')
        conn.send(base64.b64decode(data.encode('utf-8')))
        conn.close()

if __name__ == '__main__':
    NewBrioExtension().run()
