import sys
from os.path import expanduser, exists
import os
import threading
import time
import subprocess
from dataclasses import dataclass, field
from collections import defaultdict
from pprint import pprint
from typing import Optional, Any
from multiprocessing.connection import Listener
import easing_functions
from math import isclose
import bisect
import shutil
import dill as pickle
import base64

from PySide2.QtCore import QObject, Signal, Property, QUrl, Slot
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtWidgets import QApplication

import inkex
import inkex.tween
from inkex.styles import Style
from inkex.transforms import Transform

prop_names = {'x': 'x', 'y': 'y', 'Width': 'width', 'Height': 'height',
             'Stroke': 'stroke', 'Stroke width': 'stroke-width',
             'Stroke opacity': 'stroke-opacity', 'Fill': 'fill',
             'Blur': None, 'Transform': 'transform',
             'Opacity': 'opacity', 'Fill opacity': 'fill-opacity',
             'Path': 'd'}

styles = ('fill', 'stroke', 'stroke-width', 'stroke-opacity', 'fill-opacity', 'opacity')

# Ideally one day we'd save and load to SMIL directly instead
# of relying on the unreliable pickle function

def recreate_path(p):
    n = inkex.elements.PathElement()
    n.set_path(p)
    return n

@pickle.register(inkex.elements.PathElement)
def save_path(pickler, obj):
    pickler.save_reduce(recreate_path, (obj.get_path(),), obj=obj)

def ask_inkscape(properties):
    commands.append(b'love.nicco.brio')
    conn = listener.accept()
    conn.send(properties)
    return conn.recv()

def save_inkscape(obj):
    commands.append(b'love.nicco.savebrio')
    conn = listener.accept()
    conn.send(obj)
    return conn.recv()

def command_for(item):
    if isinstance(item, (float, int)): return str(item)
    elif isinstance(item, Style):
        for csskey, cssval in item.items(): return str(cssval)
    elif isinstance(item, Transform): return str(item).replace(',', ' ')
    elif isinstance(item, (inkex.Path, inkex.paths.CubicSuperPath)):
        return str(item).replace(',', ' ')
    else: assert False, type(item)

def fuzzy_equals(a, b):
    if isinstance(a, Transform):
        return all([fuzzy_equals(getattr(a, x), getattr(b, x))
                    for x in 'abcdef'])
    if isinstance(a, float): return isclose(a, b, rel_tol=1e-04)
    return a == b

def interpolate(nodes, time, fromTime, toTime, ease):
    alfa, beta = nodes[fromTime], nodes[toTime]
    k = ease((time - fromTime) / (toTime - fromTime))
    if alfa is None or beta is None: return None
    if isinstance(alfa, (float, int)): return alfa + (beta - alfa) * k
    if isinstance(alfa, Style): return alfa.interpolate(beta, k)
    if isinstance(alfa, Transform): return alfa.interpolate(beta, k)
    if isinstance(alfa, inkex.Path):
        f = inkex.tween.PathInterpolator(alfa, beta)
        f.truncate_subpaths()
        return f.interpolate(k)
    assert False, type(alfa)

def interpolate_keynodes(keynodes, time, easings):
    output = {}
    for name in keynodes:
        output[name] = {}
        for prop, nodes in keynodes[name].items():
            times = sorted(nodes)
            index = bisect.bisect_left(times, time)
            if time in nodes: output[name][prop] = nodes[time]
            elif index == 0: output[name][prop] = nodes[times[0]]
            elif index == len(times): output[name][prop] = nodes[times[-1]]
            else: output[name][prop] = interpolate(nodes, time, times[index-1], times[index],
                                                   easings[name][prop][times[index-1]])
    return output

def firstLambda(): return defaultdict(secondLambda) # Necessary to pickle
def secondLambda(): return defaultdict(easing_functions.QuadEaseInOut)

class Backend(QObject):

    def __init__(self):
        QObject.__init__(self, None)
        self._selected_els = []
        self._keynodes = {}
        self.easings = defaultdict(firstLambda)

    selectedElChanged, namesChanged = Signal(), Signal()
    propertiesChanged, keynodesChanged = Signal(), Signal()

    def get_keynodes(self):
        return {key: {skey: list(svalue.keys()) for skey, svalue in value.items()}
                for key, value in self._keynodes.items()}
    def get_selected_el(self):
        return [x for x in self._selected_els if x not in self._keynodes]
        return ""
    def get_names(self): return list(self._keynodes.keys())
    def get_properties(self):
        return {key: list(value.keys()) for key, value in self._keynodes.items()}
    def get_selected_els(self): return self._selected_els

    selected_el = Property('QVariant', fget=get_selected_el, notify=selectedElChanged)
    selected_els = Property('QVariant', fget=get_selected_els, notify=selectedElChanged)
    names = Property('QVariant', fget=get_names, notify=namesChanged)
    properties = Property('QVariant', fget=get_properties, notify=propertiesChanged)
    keynodes = Property('QVariant', fget=get_keynodes, notify=keynodesChanged)

    @Slot()
    def save(self):
        if not self._keynodes: return
        can_save = {key: value for key, value in self._keynodes.items() if pickle.pickles(value)}
        if (unsupported := self._keynodes.keys() - can_save.keys()):
            print("Couldn't save:", unsupported)
        pickled_obj = pickle.dumps({'keynodes': can_save, 'easings': self.easings})
        encoded_obj = base64.b64encode(pickled_obj).decode('utf-8')
        save_inkscape(encoded_obj)

    @Slot()
    def load(self):
        data = save_inkscape(None)
        pickled_obj = data#base64.b64decode(data.encode('utf-8'))
        obj = pickle.loads(pickled_obj)
        self._keynodes = obj['keynodes']
        self.easings = obj['easings']
        self.keynodesChanged.emit()
        self.namesChanged.emit()
        self.propertiesChanged.emit()

    @Slot(int)
    def addKeyframes(self, atTime):
        defacto = ask_inkscape({name: list(props) for name, props in self._keynodes.items()})
        dejure = interpolate_keynodes(self._keynodes, atTime, self.easings)
        for name in self._keynodes:
            for prop in self._keynodes[name]:
                if not fuzzy_equals(defacto[name][prop], dejure[name][prop]):
                    self._keynodes[name][prop][atTime] = defacto[name][prop]
        self.keynodesChanged.emit()

    @Slot(int)
    def setTime(self, time):
        target = interpolate_keynodes(self._keynodes, time, self.easings)
        selection = self._selected_els
        command = ''
        for name in target:
            command += f'select-clear;select-by-id:{name};'
            for key, value in target[name].items():
                if not key in prop_names: continue
                to_set = 'property' if prop_names[key] in styles else 'attribute'
                command += f'object-set-{to_set}:{prop_names[key]},{command_for(value)};'

        command += 'select-clear;'
        for el in selection: command += f'select-by-id:{el};'
        commands.clear()
        commands.append(bytes(command, 'utf-8'))

    @Slot(int, int, float, bool)
    def render(self, frames, framerate, speed, transparency):
        if os.path.exists('frames'):
            shutil.rmtree('frames')
        os.mkdir('frames')
        commands.append(b'export-area-page\n')
        for t in range(int(frames / speed)):
            print(t * speed)
            self.setTime(t * speed)
            while commands: time.sleep(0.01)
            commands.append(bytes(f'export-filename:frames/{t:06}.png;export-do\n', 'utf-8'))
            while not exists(f'frames/{t:06}.png'): time.sleep(0.1)
        if transparency:
            os.system(f"ffmpeg -r {framerate} -i frames/%6d.png -vcodec qtrle -pix_fmt rgba output.mov")
        else:
            os.system(f"ffmpeg -r {framerate} -f image2 -i frames/%6d.png -c:v libx264 -profile:v high -level:v 4.1 output.mp4")

    @Slot(str, str, int, str)
    def setEasingCurve(self, name, prop, time, easing):
        self.easings[name][prop][time] = getattr(easing_functions, easing)()

    @Slot(str, str, int)
    def removeKeyframe(self, name, prop, time):
        del self._keynodes[name][prop][time]
        self.keynodesChanged.emit()

    @Slot(str, str, int, int)
    def moveKeyframe(self, name, prop, fromTime, toTime):
        if fromTime == toTime: return
        self._keynodes[name][prop][toTime] = self._keynodes[name][prop][fromTime]
        self.easings[name][prop][toTime] = self.easings[name][prop][fromTime]
        del self._keynodes[name][prop][fromTime]
        self.keynodesChanged.emit()

    @Slot(str, str)
    def removeProperty(self, name, prop):
        del self._keynodes[name][prop]
        self.propertiesChanged.emit()

    @Slot(str)
    def removeObject(self, name):
        del self._keynodes[name]
        self.namesChanged.emit()

    @Slot(list, list, int)
    def addObject(self, names, properties, time):
        values = ask_inkscape({x: properties for x in names})
        for name in names:
            self._keynodes[name] = {key: {time: values[name][key]} for key in properties}
        self.namesChanged.emit()

    @Slot(list, list, int)
    def setObjectProperties(self, names, properties, time):
        values = ask_inkscape({name: properties for name in names})
        for name in names:
            for prop in properties:
                if prop not in self._keynodes[name]:
                    self._keynodes[name][prop] = {time: values[name][prop]}
            for prop in self._keynodes[name]:
                if not prop in properties:
                    del self._keynodes[name][prop]
        self.propertiesChanged.emit()

    @Slot()
    def quickPropRefresh(self):
        temp, self._keynodes = self._keynodes, {}
        self.propertiesChanged.emit()
        self._keynodes = temp
        self.propertiesChanged.emit()

def run_inkscape(commands):
    global inkscape_stdin
    thot_process = subprocess.Popen(['inkscape', '-g', '--shell'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    inkscape_stdin, index, flag = thot_process.stdin, 0, False

    def consume(this):
        out = ""
        while (c := this.read(1).decode()) != ">": out += c
        return out

    consume(thot_process.stdout)

    while True:
        index += 1
        if b'FileQuit' in commands: break
        if commands:
            thot_process.stdin.write(commands.pop() + b'\n')
            thot_process.stdin.flush()
            consume(thot_process.stdout)
        elif index > 100:
            thot_process.stdin.write(b'select-list\n')
            thot_process.stdin.flush()
            index = 0
            out = consume(thot_process.stdout).replace('select-list', '').strip()
            backend._selected_els = [line.strip().split()[0] for line in out.split('\n') if line]
            backend.selectedElChanged.emit()
        else:
            thot_process.stdin.write(b'\n')
            thot_process.stdin.flush()
            consume(thot_process.stdout)

backend = Backend()

commands, inkscape_stdin = [], None
listener = Listener(('localhost', 6123), authkey=b'brio')

t1 = threading.Thread(target=run_inkscape, args=(commands,))
t1.start()

app = QApplication(sys.argv)
engine = QQmlApplicationEngine()
engine.rootContext().setContextProperty("backend", backend)
engine.quit.connect(app.quit)
engine.load('main.qml')
app.exec_()

commands.append(b'FileQuit')
