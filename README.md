## Brio is an application for creating animations in Inkscape.

- It can animate any SVG element.
- It supports easing curves, multiple keyframes for each property, and animation previews directly in Inkscape.
- The animations are saved directly in the SVG file and can be rendered as video files or CSS animations.

**What** does Brio support?

Currently, Brio supports animations with changes in position, size, transform, path, rounded borders, transparency, fill color, and stroke. In the first version, there will also be support for gradients, mesh gradients, and text.


**How** to use Brio?

Brio animations can be rendered as videos in any resolution and frame rate. Background transparency is supported, so the final video can be overlaid onto another. The animations can also be exported as CSS animations, allowing you to embed the animated SVG directly into a web page natively.



**When** will Brio be ready?

Currently, Brio is a work in progress. There is an alpha version of Brio that is not yet publicly available.
